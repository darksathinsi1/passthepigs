from pigs import *
from time import sleep

def main() :
    
    tempo = 3
    joueur = [[None, 0, 0],
              [None, 0, 0]]
    
    joueurActif = 0
    joueurVainqueur = None
      
    joueur[0][0] = input('Quel est ton avatar joueur Zéro ? : ')
    joueur[1][0] = input('Quel est ton avatar joueur Un ? : ')
    
    joueurActif = quiJoue()
    testGagne = joueurGagne(joueur[joueurActif])
    
    while not testGagne :
        
        sleep(tempo)
        print(f'\n{joueur[0][0]} total : {joueur[0][1]}\n{joueur[1][0]} total : {joueur[1][1]}\n')   
        sleep(tempo)
        print(f"C'est {joueur[joueurActif][0]} qui joue.\n")
        lancer = alea2Pigs(statPig5000)
        sleep(tempo)
        print(f'{joueur[joueurActif][0]} a obtenu la combinaison {positionsPig[lancer[0]]}|{positionsPig[lancer[1]]}.')
        print(f'{joueur[joueurActif][0]} cette combinaison te rapporte {points(lancer)} point(s).\n')
        sleep(tempo)
        
        if est_ce_que_caGrouine(caGrouine, 6000):
            print(f'{joueur[joueurActif][0]} tes cochons se touchent, ton total de points est 0.')
            sleep(tempo)
            print('Tu passes les cochons\n')
            joueur[joueurActif][1] = 0
            joueur[joueurActif][2] = 0
            joueurActif = passLesPigs(joueurActif)
        elif combinaisonZero(lancer) :
            print(f'{joueur[joueurActif][0]} ta combinaison est de zéro point.\n')
            sleep(tempo)
            print('Tu passes les cochons ........')
            joueur[joueurActif][2] = 0
            joueurActif = passLesPigs(joueurActif)           
        else :
            print('Taper 1 pour sauver tes points 0 sinon')
            choixJoueur = int(input(f'{joueur[joueurActif][0]} veux-tu sauver tes points ? : '))
           
            if choixJoueur == 1 :
                sleep(tempo)                             
                cumulerPoints(joueur[joueurActif], points(lancer))
                print(f'\n{joueur[joueurActif][0]} a sauvé {joueur[joueurActif][2]} point(s).')  
                sauverLesPoints(joueur[joueurActif])
                print(f'Ton total de points est de {joueur[joueurActif][1]} points.\n')
                testGagne = joueurGagne(joueur[joueurActif])
                joueurVainqueur = joueurActif
                joueurActif = passLesPigs(joueurActif)                                
            elif choixJoueur == 0 :
                cumulerPoints(joueur[joueurActif], points(lancer))
                sleep(tempo)
                print(f'{joueur[joueurActif][0]} a cumulé {joueur[joueurActif][2]} point(s).\n')
    
    print(f'{joueur[joueurVainqueur][0]} est le vainqueur du PASS THE PIGS avec {joueur[joueurVainqueur][1]} point(s).\n')