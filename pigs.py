from random import randint

positionsPig = ['FLANC GAUCHE', 'FLANC DROIT',
                'TOURNEDOS', 'TROTTEUR', 'GROIN-GROIN', 'BAJOUE']

statPig5000 = [1739, 1431, 1256, 436, 97, 41]

stat2Pigs6000 = [[573, 656, 360, 139, 56, 12],
                 [623, 731, 449, 185, 58, 17],
                 [396, 473, 308, 124, 45, 8],
                 [155, 180, 149, 45, 17, 5],
                 [54, 67, 47, 13, 2, 1],
                 [10, 10, 7, 0, 1, 1]]

points2Pigs = [[1, 0, 5, 5, 10, 15],
               [0, 1, 5, 5, 10, 15],
               [5, 5, 20, 10, 15, 20],
               [5, 5, 10, 20, 15, 20],
               [10, 10, 15, 15, 40, 25],
               [15, 15, 20, 20, 25, 60]]

caGrouine = 23

joueurs = [[0, 0], [0, 0]]


def quiJoue():
    '''
    La fonction quiJoue doit retourner un entier de l’ensemble {0, 1}.
    '''


def aleaPig(stat):
    ''' 
    Tirage d'un cochon avec prise en compte des stats
    '''


def alea2Pigs(stat):
    ''' 
    Tirage de deux cochons avec prise en compte des stats
    '''


def points(tbl2pigs):
    ''' 
    Cette fonction retourne les points d’une combinaison de deux cochons
    '''


def cumulerPoints(joueur, points):
    joueur[2] += points


def est_ce_que_caGrouine(favorables, total):  # 23|6000
    ''' 
    Détermination aléatoire de la position CaGrouine(Les cochons se touchent)
    '''


def combinaisonZero(tbl2pigs):
    ''' 
    Est-ce que l’en a une positions des cochons à zéro points?
    '''


def passLesPigs(joueurActif):
    '''
    On change le joueur.
    '''


def sauverLesPoints(joueur):
    '''
    On sauve les points cumulés du joueur.
    '''


def joueurGagne(joueur):
    '''
    On détermine si le joueur a gagné.
        '''


if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE |
                    doctest.ELLIPSIS, verbose=True)
