# ![pig](images/pig.png)[PassThePigs](pass_the_Pigs.pdf)

# ![pig](images/pig.png)[Le fichier des fonctions du Pass The Pigs](pigs.py)

**Il faut réaliser toutes les DOCSTRING et DOSTEST des fonctions du module pigs.py**

# ![pig](images/pig.png)[Le fichier Interface du Pass The Pigs](interfacePigs.py)

**Il vous appartient d'améliorer cette interface.**

# ![pig](images/pig.png)[Le fichier Interface graphique Tkinter du Pass The Pigs](pigsTkinter.py)

**Il faut réaliser votre propre interface en réalisant un wireframe de votre projet**

![wireframing](images/wireframingPigs.png)

![interface](images/interface.png)

# ![pig](images/pig.png)[Télécharger DIAW](https://www.dropbox.com/s/hdoqkghiohwvke1/DiaPortable.zip?dl=0)

![organigramme](images/PassThePigs.png)

