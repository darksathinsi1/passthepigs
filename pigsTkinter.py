import tkinter.ttk as ttk
import tkinter.font as font
from tkinter import *
from PIL import Image, ImageTk

image = Image.open("pigsTkinter2.png")
image = image.resize((200,200), Image.ANTIALIAS)

def regles():
    windowRegles = Toplevel()
    windowRegles.geometry('400x400+1000+600')

# Création d'une première fenêtre
window = Tk()

# Choix aléatoire d'une police
font_tbl = [i for i in font.families()]

# Personnaliser la fenêtre
window.title('Pass The PIGS')

# widthxheight+positionX+positionY
window.geometry('400x600+1200+600')

# Dimensions minimales de la fenêtre
window.minsize(300,300)

# Dimensions maximales de la fenêtre
window.maxsize(600,600)

# Interdire le redimensionnement de la fenêtre(width, height)
window.resizable(False, False)

# Personnaliser l'icône de la fenêtre
window.iconbitmap("logoPig.ico")

# Personnaliser la couleur du background : https://www.colorhexa.com
window.config(bg='#efa1d5')

# Création d'une Frame
zone = Frame(window, bg='#efa1d5', bd=0) # flat, groove, raised, ridge, solid, ou sunken

# Ajouter le texte titre à sa fenêtre applicative
style = ttk.Style()
style.configure("Dark.TLabel", foreground="#fff", background="#efa1d5", font=("Alice and the Wicked Monster", 35, 'bold'))
titre = ttk.Label(zone, text="PASS THE PIGS", style ="Dark.TLabel")
auteur = Label(zone, text="By DarkSATHI", bg='#efa1d5', font=("Helvetica", 15), fg="#fff")

# Texte d'échanges avec le(s) joueur(s) humains
texte = StringVar()
texte.set("Le Pass The Pigs n'a pas commencé.")
jeuInfos = Label(window,
                 text=texte.get(),
                 bg='#efa1d5',
                 font=("Helvetica", 15),
                 fg="#fff")

# Les boutons du jeu
style.configure("fun.TButton",
                foreground="#efa1d5",
                background="#fff",
                font=(font_tbl[15], 15, 'bold'))
bt_joueur1 = ttk.Button(window,
                        text="Joueur 1",
                        style="fun.TButton")
bt_joueur2 = ttk.Button(window,
                        text="Joueur 2",
                        style="fun.TButton" )

# Création d'un menu
menuBar = Menu(window)
window.config(menu=menuBar)
menuJouer = Menu(menuBar,tearoff=0)
menuBar.add_cascade(label="Jouer", menu=menuJouer)
menuJouer.add_command(label="Commencer", command=None)
menuJouer.add_command(label="Les Règles", command=regles)
menuJouer.add_separator() 
menuJouer.add_command(label="Quitter", command=window.destroy)

# Image d'illustration
MON_ILLUSTRATION = ImageTk.PhotoImage(image)
illustration = Canvas(window,
                      width=200,
                      height=200,
                      bg="#efa1d5",
                      highlightbackground="#efa1d5",
                      highlightcolor="#efa1d5",
                      highlightthickness=0,
                      bd=0)

id_illustration = illustration.create_image(0, 0, anchor='nw', image=MON_ILLUSTRATION) 

def entreeJoueur(fenetre, variable):
    return Entry(fenetre, textvaniable=variable, state=DISABLED)

# Empaquetage des éléments dans la fenêtre
titre.pack(side = "top", expand=False, pady=5, padx=5) # top, bottom, left, ou right
auteur.pack(side = "top", expand=False, pady=5, padx=5)
zone.pack(expand=False)
illustration.pack(ipadx = 0,ipady = 0, pady=15)
jeuInfos.pack(pady=15)
bt_joueur1.pack(pady=5)
bt_joueur2.pack(pady=5)
window.mainloop()